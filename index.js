const posts = [
    {
        name: "Vincent van Gogh",
        username: "vincey1853",
        location: "Zundert, Netherlands",
        avatar: "images/avatar-vangogh.jpg",
        post: "images/post-vangogh.jpg",
        comment: "just took a few mushrooms lol",
        likes: 21
    },
    {
        name: "Gustave Courbet",
        username: "gus1819",
        location: "Ornans, France",
        avatar: "images/avatar-courbet.jpg",
        post: "images/post-courbet.jpg",
        comment: "i'm feelin a bit stressed tbh",
        likes: 4
    },
    {
        name: "Joseph Ducreux",
        username: "jd1735",
        location: "Paris, France",
        avatar: "images/avatar-ducreux.jpg",
        post: "images/post-ducreux.jpg",
        comment: "gm friends! which coin are YOU stacking up today?? post below and WAGMI!",
        likes: 152
    }
]

const postEl = document.getElementById("post-el")

for (let i = 0; i < posts.length; i++) {
    postEl.innerHTML += `
    <section class="main-header p-5">
        <img class="avatar-img" src=${posts[i].avatar}>
            <div class="user-info">
                <p id="name-el" class="bolder">${posts[i].name}</p>
                <p id="location-el">${posts[i].location}</p>
            </div>
    </section>
    <img class="main-img" src=${posts[i].post}>
    <section class="likes-section">
        <div class="icon-section">
            <img class="icon" src="images/icon-heart.png">
            <img class="icon" src="images/icon-comment.png">
            <img class="icon" src="images/icon-dm.png">
        </div>
        <p id="likes-el" class="bolder">${posts[i].likes} likes</p>
        <p id="comment-el"><span class="bolder"> ${posts[i].username} </span>  ${posts[i].comment}</p>
    </section>
    `
}
